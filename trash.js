var due = cash - price;
var funds = cid[0][1] * 1 + cid[1][1] * 2 + cid[2][1] * 5 + cid[3][1] * 10 + cid[4][1] * 20 + cid[5][1] * 100 + cid[6][1] * 1000;
if (funds < due)
  return "Insufficient Funds";
else if (funds === due) {
  return "Closed";
}
else {
  var change = [];
  var thousands = Math.floor(due/1000);
  if(thousands >= 1) {
    change.push(['ONE THOUSAND RUPEES', thousands]);
    due = due - thousands*1000;
  }
  var hundreds = Math.floor(due/100);
  if(hundreds >= 1) {
    change.push(['ONE HUNDRED RUPEES', hundreds]);
    due = due - hundreds*100;
  }
  var twenties = Math.floor(due/20);
  if(twenties >= 1) {
    change.push(['TWENTY RUPEES', twenties]);
    due = due - twenties*20;
  }
  var tens = Math.floor(due/10);
  if(tens >= 1) {
    change.push(['TEN RUPEES', tens]);
    due = due - tens*10;
  }
  var fives = Math.floor(due/5);
  if(fives >= 1) {
    change.push(['FIVE RUPEES', fives]);
    due = due - fives*5;
  }
  var twos = Math.floor(due/2);
  if(twos >= 1) {
    change.push(['TWO RUPEES', twos]);
    due = due - twos*2;
  }
  var ones = Math.floor(due/1);
  if(ones >= 1) {
    change.push(['ONE RUPEE', ones]);
    due = due - ones*1;
  }
  return change;
}
