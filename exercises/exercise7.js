/*
 *  Write a JavaScript function that accepts a string as a parameter and converts the first letter
 *  of each word of the string in upper case.
 */

var upperCase = function(word) {
  if(word.length === 0) {
    return "";
  }
  else {
    var words = word.split(" ");
    for(i=0;i<words.length;i++) {
      words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
    }
    word = words.join(" ");
    return word;
  }
};
