/*
 *  Write a JavaScript function that checks whether a passed string is palindrome or not.
 */

var isPalindrome = function(word) {
  var reversedWord = word.split("").reverse().join("");
  if(reversedWord === word)
    return true;
  return false;
};
