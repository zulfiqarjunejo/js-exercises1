/**
 * Given two input strings, write a function to find largest identical substring
 * For example:
 * String
 * Spring
 * Here the largest substring is 'ring'
 *
 * In case there are multiple largest substrings of similar length, the function must return the first largest substring found w.r.t first string, For example:
 * hirespring
 * ringfhire
 * Here, the largest substring returned will be 'hire' and NOT 'ring'
*/

function largestSubstring(str1, str2) {
  var len1 = str1.length;
  var len2 = str2.length;
  var requiredSubstring = "";
  var lengthOfLS = 0;
  var posofLS=0;
	for(i=1;i<=len2;i++) {
    for(j=0;j<len2-i+1;j++) {
      //console.log(str2.substr(j, i));
      var subString = str2.substr(j, i);
      var pos = str1.search(subString);
      if(pos != -1) {
        var length = subString.length;
        if(length > lengthOfLS) {
          posofLS = pos;
          console.log("Largest subString till now is: " + subString);
          requiredSubstring = subString;
          lengthOfLS = length;
        }
        else if (length === lengthOfLS){
          if(posofLS > pos){
            posofLS = pos;
            console.log("Largest subString till now is: " + subString);
            requiredSubstring = subString;
            lengthOfLS = length;
          }
        }
      }
    }
  }
  return requiredSubstring;
}
