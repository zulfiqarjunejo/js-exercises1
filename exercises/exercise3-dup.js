/*
 *  Define a function reverse() that computes the reversal of a (full) string. For example,
 *  reverse("jag testar") should return the string "ratset gaj".
 */

var reverse = function(words) {
  if(words === '')
    return '';
  else {
    var reversedWord = "";
    var length = words.length;
    for(i=length-1;i>=0;i--){
      reversedWord += words.charAt(i);
    }
    return reversedWord;
  }
};
