/*
 *  Write a JavaScript program to validate a date in dd/mm/yyyy format. If the user input matches with
 *  the format, the program will return a message "Valid Date" otherwise return a message "Invalid Date!"
 */

var checkDate = function(dateStr) {
  var pattern = new RegExp("^(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[0-2])/(19|20)\\d\\d");
  var isValid = pattern.test(dateStr);
  if(isValid)
    return "Valid Date";
  return "Invalid Date!";
};
