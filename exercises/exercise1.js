/*
 * Write a function findLongestWord() that takes an array of words and returns the length of the longest word.
 *
 */

var findLongestWord = function(words) {
  var lengthOfLongestWord = 0;
  var numberOfWords = words.length;
  if(numberOfWords === 0)
    return lengthOfLongestWord;
  else {
    lengthOfLongestWord = words[0].length;
    for(i=1;i<numberOfWords;i++) {
      if(words[i].length > lengthOfLongestWord) {
        lengthOfLongestWord = words[i].length;
      }
    }
    return lengthOfLongestWord;
  }
};
