/*
*  Design a cash register drawer function that accepts purchase * price as the first argument, payment as
*  the second argument, and cash-in-drawer (cid) as the third argument.
*
*  cid is a 2d array listing available currency.
*
*  Return the string "Insufficient Funds" if cash-in-drawer is less than the change due. Return the
*  string "Closed" if cash-in-drawer is equal to the change due.
*
*  Otherwise, return change in coin and bills, sorted in highest to lowest order.
*  In case of multiple solutions, return the one containing the highest value of coin/bill
*/

function drawer(price, cash, cid) {
	var myCID = {};
	myCID.ONE_RUPEE = cid[0][1];
	myCID.TWO_RUPEES = cid[1][1];
	myCID.FIVE_RUPEES = cid[2][1];
	myCID.TEN_RUPEES = cid[3][1];
	myCID.TWENTY_RUPEES = cid[4][1];
	myCID.ONE_HUNDRED_RUPEES = cid[5][1];
	myCID.ONE_THOUSAND_RUPEES = cid[6][1];

	var due = cash - price;
	var fundsAvailable = myCID.ONE_RUPEE * 1 + myCID.TWO_RUPEES * 2 + myCID.FIVE_RUPEES * 5 + myCID.TEN_RUPEES * 10 + myCID.TWENTY_RUPEES * 20 + myCID.ONE_HUNDRED_RUPEES * 100 + myCID.ONE_THOUSAND_RUPEES * 1000;

	console.log("Due = " + due);
	console.log("Funds Available = " + fundsAvailable);

	if(fundsAvailable < due) {
		return "Insufficient Funds";
	}
	else if (fundsAvailable === due) {
		return "Closed";
	}
	else {
		var change = [];
		var thousands = Math.floor(due/1000);
		if(thousands>=1) {
			if(thousands<=myCID.ONE_THOUSAND_RUPEES) {
				change.push(['ONE THOUSAND RUPEES', thousands]);
				due = due-thousands*1000;
			}
			else {
				change.push(['ONE THOUSAND RUPEES', myCID.ONE_THOUSAND_RUPEES]);
				due = due-myCID.ONE_THOUSAND_RUPEES*1000;
			}
		}
		var hundreds=Math.floor(due/100);
		if(hundreds>=1) {
			if(hundreds<=myCID.ONE_HUNDRED_RUPEES) {
				change.push(['ONE HUNDRED RUPEES', hundreds]);
				due = due-hundreds*100;
			}
			else {
				change.push(['ONE HUNDRED RUPEES', myCID.ONE_HUNDRED_RUPEES]);
				due = due - myCID.ONE_HUNDRED_RUPEES*100;
			}
		}
		var twenties = Math.floor(due/20);
		if(twenties >= 1) {
			if(twenties <= myCID.TWENTY_RUPEES) {
				change.push(['TWENTY RUPEES', twenties]);
				due = due - twenties*20;
			}
			else {
				change.push(['TWENTY RUPEES', myCID.TWENTY_RUPEES]);
				due = due - myCID.TWENTY_RUPEES*20;
			}
		}
		var tens = Math.floor(due/10);
		if(tens >= 1) {
			console.log("TENS = " + tens);
			if(tens <= myCID.TEN_RUPEES) {
				console.log("tens <= myCID.TEN_RUPEES");
				change.push(['TEN RUPEES', tens]);
				due = due - tens*10;
			}
			else {
				change.push(['TEN RUPEES', myCID.TEN_RUPEES]);
				due = due - myCID.TEN_RUPEES*10;
			}
		}
		var fives = Math.floor(due/5);
		if(fives >= 1) {
			if(fives <= myCID.FIVE_RUPEES) {
				change.push(['FIVE RUPEES', fives]);
				due = due - fives*5;
			}
			else {
				change.push(['FIVE RUPEES', myCID.FIVE_RUPEES]);
				due = due - myCID.FIVE_RUPEES*5;
			}
		}
		var twos = Math.floor(due/2);
		if(twos >= 1) {
			if(twos <= myCID.TWO_RUPEES) {
				change.push(['TWO RUPEES', twos]);
				due = due - twos*2;
			}
			else {
				change.push(['TWO RUPEES', myCID.TWO_RUPEES]);
				due = due - myCID.TWO_RUPEES*2;
			}
		}
		var ones=Math.floor(due/1);
		if(ones>=1) {
			if(ones<=myCID.ONE_RUPEE) {
				change.push(['ONE RUPEE', ones]);
				due=due-ones*1;
			}
			else {
				change.push(['ONE RUPEE', myCID.ONE_RUPEE]);
				due=due-myCID.ONE_RUPEE*1;
			}
		}
		return change;
	}
}


// Example cash-in-drawer array:
// ['ONE RUPEE', 10],
// ['TWO RUPEES', 15],
// ['FIVE RUPEES', 35],
// ['TEN RUPEES', 9],
// ['TWENTY RUPEES', 8],
// ['ONE HUNDRED RUPEES', 5],
// ['ONE THOUSAND RUPEES', 2]]

// The currency of the cash in drawer can always assumed to be PKR with denominations in the example above

// Example call
// drawer(17, 20, [['ONE RUPEE', 11], ['TWO RUPEES', 15], ['FIVE RUPEES', 36], ['TEN RUPEES', 9], ['TWENTY RUPEES', 8], ['ONE HUNDRED RUPEES', 5], ['ONE THOUSAND RUPEES', 2]]);
// output [['TWO RUPEES', 1], [ONE RUPEE', 1]]
