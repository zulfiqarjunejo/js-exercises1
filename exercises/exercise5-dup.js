/*
 *  Write a JavaScript function that checks whether a passed string is palindrome or not.
 */

var isPalindrome = function(word) {
  var i=0, j=word.length-1;
  while(i<=j) {
    if(word.charAt(i) != word.charAt(j))
      return false;
    i++;
    j--;
  }
  return true;
};
