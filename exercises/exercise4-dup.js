/*
 *  Write a function to sort a list of words (an array) in alphabetical order
 */

var sort = function(words) {
  var numberOfWords = words.length;
  for(i=0;i<numberOfWords;i++) {
    for(j=i+1;j<numberOfWords;j++) {
      if(words[i] > words[j]) {
        var temp = words[i];
        words[i] = words[j];
        words[j] = temp;
      }
    }
  }
  return words;
};
