/*
 *  Write a function filterLongWords() that takes an array of words and an integer i and returns the array
 *  of words that are longer than i.
 */

var filterLongWords = function(words, i) {
  var numberOfWords = words.length;
  if(numberOfWords === 0)
    return [];
  else {
    var longWords = [];
    for(j=0;j<numberOfWords;j++){
      if(words[j].length > i) {
        longWords.push(words[j]);
      }
    }
    return longWords;
  }
};
